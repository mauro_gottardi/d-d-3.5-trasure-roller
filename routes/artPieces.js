var express = require('express');
var router = express.Router();

var artPieces = require('../bin/lib/artPieces');

/* GET art pieces */

router.get('/all', function(req, res, next){
  res.json(artPieces.all());
})

router.get('/random', function(req, res, next){
  res.json(artPieces.getRandomArtPiece());
})

router.get('/number', function(req, res, next){

  var number = req.query['n'];
  var json = null;

  if(number !== undefined && number !== null && Number(number) !== NaN ){
    
    var artPiecesArray = artPieces.getSpecificNumberOfArtPieces(number);
    json = { 'stauts': 'true', 'result': artPiecesArray };

  } else {
    json = { 'status': 'false' };
  }

  res.json(json);

})

module.exports = router;
