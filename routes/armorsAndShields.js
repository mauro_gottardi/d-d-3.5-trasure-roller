var express = require('express');
var router = express.Router();

var armorsAndShields = require('../bin/lib/randomItemTables/armorsAndShields');

/* GET art pieces */

router.get('/all', function(req, res, next){
  res.json(armorsAndShields.all());
})

router.get('/getRandomArmorOrShield', function(req, res, next){

  var rarity = req.query['rarity'] || 'minor';

  res.json(armorsAndShields.getRandomArmorOrShield(rarity));
})

module.exports = router;
