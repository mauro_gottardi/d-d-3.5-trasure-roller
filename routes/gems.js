var express = require('express');
var router = express.Router();

var gems = require('../bin/lib/gems');

/* GET gem */

router.get('/all', function(req, res, next){
  res.json(gems.all());
})

router.get('/random', function(req, res, next){
  res.json(gems.getRandomGem());
})

router.get('/number', function(req, res, next){

  var number = req.query['n'];
  var json = null;

  if(number !== undefined && number !== null && Number(number) !== NaN ){
    
    var gemsArray = gems.getSpecificNumberOfGems(number);
    json = { 'stauts': 'true', 'result': gemsArray };

  } else {
    json = { 'status': 'false' };
  }

  res.json(json);

})

module.exports = router;
