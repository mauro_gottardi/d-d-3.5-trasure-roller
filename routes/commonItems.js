var express = require('express');
var router = express.Router();

var commonItems = require('../bin/lib/commonItems');

/* GET gem */

router.get('/all', function(req, res, next){
  res.json(commonItems.all());
})

router.get('/random', function(req, res, next){
  res.json(commonItems.getRamdomCommonItem());
})

router.get('/number', function(req, res, next){

  var number = req.query['n'];
  var json = null;

  if(number !== undefined && number !== null && Number(number) !== NaN ){
    
    var commonItemsArray = commonItems.getSpecificNumberOfCommonItem(number);
    json = { 'stauts': 'true', 'result': commonItemsArray };

  } else {
    json = { 'status': 'false' };
  }

  res.json(json);

})

module.exports = router;
