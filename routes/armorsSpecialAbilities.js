var express = require('express');
var router = express.Router();

var armorsSpecialAbilities = require('../bin/lib/randomItemTables/armorsSpecialAbilities');

/* GET art pieces */

router.get('/all', function(req, res, next){
  res.json(armorsSpecialAbilities.all());
})

router.get('/getRandomAbilities', function(req, res, next){

  var bonus = req.query['bonus'] || 1;
  var rarity = req.query['rarity'] || 'minor';

  res.json(armorsSpecialAbilities.getRandomAbilities(rarity, bonus));
})

module.exports = router;
