var express = require('express');
var router = express.Router();

var shieldsSpecialAbilities = require('../bin/lib/randomItemTables/shieldsSpecialAbilities');

/* GET art pieces */

router.get('/all', function(req, res, next){
  res.json(shieldsSpecialAbilities.all());
})

router.get('/getRandomAbilities', function(req, res, next){

  var bonus = req.query['bonus'] || 1;
  var rarity = req.query['rarity'] || 'minor';

  res.json(shieldsSpecialAbilities.getRandomAbilities(rarity, bonus));
})

module.exports = router;
