exports.item = function(num){

  var number = num || false;

  if(number){
    return alchimicItems[number];
  } else {
    return alchimicItems;
  }

}

var alchemicItems = {
  1 : 'Fuoco dell\'alchimista (1d4 ampolle, 20mo ciascuna)',
  2 : 'Acido (2d4 ampolle, 10mo ciascuna)',
  3 : 'Bastoni di fumo (1d4 bastoni, 20mo ciascuna)',
  4 : 'Acquasanta (1d4 ampolle, 25mo ciascuna)',
  5 : 'Antitossina (1d4 dosi, 50mo ciascuna)',
  6 : 'Torcia perenne',
  7 : 'Borse dell\'impedimento (1d4 borse, 50mo ciascuna)',
  8 : 'Pietre del tuono (1d4 pietre, 30mo ciascuna)'
}