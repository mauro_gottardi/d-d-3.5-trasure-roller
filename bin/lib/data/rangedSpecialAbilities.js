exports.abilities = function(){
  return rangedSpecialAbilities;
}

exports.ability = function(num){

  var number = num || false;

  if(number){
    return rangedSpecialAbilities[number];
  } else {
    return rangedSpecialAbilities;
  }

}

var rangedSpecialAbilities = {
  1  : {name: 'Anatema', bonus: 1},
  2  : {name: 'Distanza', bonus: 1},
  3  : {name: 'Infuocata', bonus: 1},
  4  : {name: 'Gelida', bonus: 1},
  5  : {name: 'Pietosa', bonus: 1},
  6  : {name: 'Ritornante', bonus: 1},
  7  : {name: 'Folgorante', bonus: 1},
  8  : {name: 'Ricercante', bonus: 1},
  9  : {name: 'Tonante', bonus: 1},
  10  : {name: 'Anarchia', bonus: 2},
  11  : {name: 'Assoiomatica', bonus: 2},
  12  : {name: 'Esplosione di fiamme', bonus: 2},
  13  : {name: 'Sacra', bonus: 2},
  14  : {name: 'Esplosione di ghiaccio', bonus: 2},
  15  : {name: 'Esplosione folgorante', bonus: 2},
  16  : {name: 'Sacrilega', bonus: 2},
  17  : {name: 'Velocità', bonus: 3},
  18  : {name: 'Energia luminosa', bonus: 4},
  19  : {name: 'Reroll two times'}
}