exports.item = function(num){

  var number = num || false;

  if(number){
    return equipmentItems[number];
  } else {
    return equipmentItems;
  }

}

var equipmentItems = {
  1 : 'Zaino, vuoto (2mo)',
  2 : 'Piede di porco (2mo)',
  3 : 'Lanterna a lente sporgente (12mo)',
  4 : 'Serratura semplice (20mo)',
  5 : 'Serratura media (40mo)',
  6 : 'Serratura buona (60mo)',
  7 : 'Serratura eccezionale (150mo)',
  8 : 'Manette perfette (50mo)',
  9 : 'Specchio piccolo di metallo (10mo)',
  10 : 'Corda di seta (15m)(10mo)',
  11 : 'Cannocchiale (1000mo)',
  12 : 'Arnesi di artigiano perfetti (55mo)',
  13 : 'Attrezzi da scalatore (80mo)',
  14 : 'Trucchi per il cammuffamento (50mo)',
  15 : 'Borsa da guaritore (50mo)',
  16 : 'Simbolo sacro d\'argento (25mo)',
  17 : 'Clessidra (25mo)',
  18 : 'Lente d\'ingrandimento (100mo)',
  19 : 'Strumento musicale perfetto (100mo)',
  20 : 'Arnesi da scasso perfetti (50mo)'
}