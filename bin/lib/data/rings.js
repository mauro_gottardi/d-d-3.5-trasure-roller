exports.all = function(){
  return rings;
}

exports.getOne = function(num){

  var number = num || false;

  if(number){
    return rings[number];
  } else {
    return rings;
  }

}

var rings = {
  1  : {name: 'Protezione +1', value: 2000},
  2  : {name: 'Caduta morbida', value: 2200},
  3  : {name: 'Sostentamento', value: 2500},
  4  : {name: 'Scalare', value: 2500},
  5  : {name: 'Saltare', value: 2500},
  6  : {name: 'Nuotare', value: 2500},
  7  : {name: 'Controincantesimo', value: 4000},
  8  : {name: 'Scudo mentale', value: 8000},
  9  : {name: 'Protezione +2', value: 8000},
  10  : {name: 'Scudo di forza', value: 8500},
  11  : {name: 'Ariete', value: 8600},
  12  : {name: 'Scalare, migliorato', value: 10000},
  13  : {name: 'Saltare, migliorato', value: 10000},
  14  : {name: 'Nuotare, migliorato', value: 10000},
  15  : {name: 'Amicizia con gli animale', value: 10800},
  16  : {name: 'Resistenza all\'energia, minore', value: 12000},
  17  : {name: 'Potere del camaleonte', value: 12700},
  18  : {name: 'Camminare sull\'acqua', value: 15000},
  19  : {name: 'Protezione +3', value: 18000},
  20  : {name: 'Accumula incantesimi, minore', value: 18000},
  21  : {name: 'Invisibilità', value: 20000},
  22  : {name: 'Stregoneria (I)', value: 20000},
  23  : {name: 'Eludere', value: 25000},
  24  : {name: 'Vista a raggi X', value: 25000},
  25  : {name: 'Intermittenza', value: 27000},
  26  : {name: 'Resistenza all\'energia, maggiore', value: 28000},
  27  : {name: 'Protezione +4', value: 32000},
  28  : {name: 'Stregoneria (II)', value: 40000},
  29  : {name: 'Libertà di movimento', value: 40000},
  30  : {name: 'Resistenza all\'energia', value: 44000},
  31  : {name: 'Scudo amico (coppia)', value: 50000},
  32  : {name: 'Protezione +5', value: 50000},
  33  : {name: 'Stelle cadenti', value: 50000},
  34  : {name: 'Accumula incantesimi', value: 50000},
  35  : {name: 'Stregoneria (III)', value: 70000},
  36  : {name: 'Telecinesi', value: 75000},
  37  : {name: 'Rigenerazione', value: 90000},
  38  : {name: 'Tre desideri', value: 97950},
  39  : {name: 'Rifletti incantesimo', value: 98280},
  40  : {name: 'Stregoneria (IV)', value: 100000},
  41  : {name: 'Richiamo del djinni', value: 125000},
  42  : {name: 'Comando degli elementi (acqua)', value: 200000},
  43  : {name: 'Comando degli elementi (aria)', value: 200000},
  44  : {name: 'Comando degli elementi (fuoco)', value: 200000},
  45  : {name: 'Comando degli elementi (terra)', value: 200000},
  46  : {name: 'Accumula incantesimo, maggiore', value: 200000}
}