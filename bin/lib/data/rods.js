exports.all = function(){
  return rods;
}

exports.getOne = function(num){

  var number = num || false;

  if(number){
    return rods[number];
  } else {
    return rods;
  }

}

var rods = {
  1  : {name: 'Metamagia, Ingrandimento, minore', value: 3000},
  2  : {name: 'Metamagia, Estensione, minore', value: 3000},
  3  : {name: 'Metamagia, Silenzio, minore', value: 3000},
  4  : {name: 'Inamovibile', value: 5000},
  5  : {name: 'Metamagia, Potenziamento, minore', value: 9000},
  6  : {name: 'Individiazione dei metalli e dei minerali', value: 10500},
  7  : {name: 'Cancellazione', value: 11000},
  8  : {name: 'Metamagia, Ingrandimento', value: 11000},
  9  : {name: 'Metamagia, Estensione', value: 11000},
  10 : {name: 'Metamagia, Silenzio', value: 11000},
  11 : {name: 'Meraviglia', value: 12000},
  12 : {name: 'Pitone', value: 13000},
  13 : {name: 'Metamagia, Massimizzazione, minore', value: 14000},
  14 : {name: 'Estinzione delle fiamme', value: 15000},
  15 : {name: 'Vipera', value: 19000},
  16 : {name: 'Individiazione dei nemici', value: 23500},
  17 : {name: 'Metamagia, Ingrandimento, superiore', value: 24500},
  18 : {name: 'Metamagia, Estinzione, superiore', value: 24500},
  19 : {name: 'Metamagia, Silenzio, superiore', value: 24500},
  20 : {name: 'Splendore', value: 25000},
  21 : {name: 'Deperimento', value: 25000},
  22 : {name: 'Metamagia, Potenziamento', value: 32500},
  23 : {name: 'Tuoni e fulmini', value: 33000},
  24 : {name: 'Metamagia, Rapidità, minore', value: 35000},
  25 : {name: 'Nagazione', value: 37000},
  26 : {name: 'Assorbimento', value: 50000},
  27 : {name: 'Mazzafrusto', value: 50000},
  28 : {name: 'Metamagia, Massimizzazione', value: 54000},
  29 : {name: 'Sovranità', value: 60000},
  30 : {name: 'Sicurezza', value: 61000},
  31 : {name: 'Potenza', value: 70000},
  32 : {name: 'Metamagia, Potenziamento, superiore', value: 73000},
  33 : {name: 'Metamagia, Rapidità', value: 75500},
  34 : {name: 'Allerta', value: 85000},
  35 : {name: 'Metamagia, Massimizzazione, superiore', value: 121500},
  36 : {name: 'Metamagia, Rapidità, superiore', value: 170000}
}