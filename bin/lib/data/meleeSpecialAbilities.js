exports.abilities = function(){
  return meleeSpecialAbilities;
}

exports.ability = function(num){

  var number = num || false;

  if(number){
    return meleeSpecialAbilities[number];
  } else {
    return meleeSpecialAbilities;
  }

}

var meleeSpecialAbilities = {
  1  : {name: 'Anatema', bonus: 1},
  2  : {name: 'Difensiva', bonus: 1},
  3  : {name: 'Infuocata', bonus: 1},
  4  : {name: 'Gelida', bonus: 1},
  5  : {name: 'Folgorante', bonus: 1},
  6  : {name: 'Tocco fantasma', bonus: 1},
  7  : {name: 'Affilata', bonus: 1},
  8  : {name: 'Focalizza ki', bonus: 1},
  9  : {name: 'Pietosa', bonus: 1},
  10  : {name: 'Incalzare rafforzato', bonus: 1},
  11  : {name: 'Accumula incantesimi', bonus: 1},
  12  : {name: 'Lancio', bonus: 1},
  13  : {name: 'Tonante', bonus: 1},
  14  : {name: 'Immorale', bonus: 1},
  15  : {name: 'Anarchia', bonus: 2},
  16  : {name: 'Assoiomatica', bonus: 2},
  17  : {name: 'Distruzione', bonus: 2},
  18  : {name: 'Esplosione di fiamme', bonus: 2},
  19  : {name: 'Esplosione di ghiaccio', bonus: 2},
  20  : {name: 'Sacra', bonus: 2},
  21  : {name: 'Esplosione folgorante', bonus: 2},
  22  : {name: 'Sacrilega', bonus: 2},
  23  : {name: 'Ferimento', bonus: 2},
  24  : {name: 'Velocità', bonus: 3},
  25  : {name: 'Energia luminosa', bonus: 4},
  26  : {name: 'Danzante', bonus: 4},
  27  : {name: 'Vorpal', bonus: 5},
  28  : {name: 'Reroll two times'}
}