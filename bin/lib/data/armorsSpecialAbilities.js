exports.abilities = function(){
  return armorsSpecialAbilities;
}

exports.ability = function(num){

  var number = num || false;

  if(number){
    return armorsSpecialAbilities[number];
  } else {
    return armorsSpecialAbilities;
  }

}

var armorsSpecialAbilities = {
  1  : {name: 'Mascheramento', value: 2700},
  2  : {name: 'Fortificazione, leggera', rank: 1, bonus: 1},
  3  : {name: 'Scivolosa', rank: 1, value: 3750},
  4  : {name: 'Ombra', rank: 1, value: 3750},
  5  : {name: 'Movimento silenzioso', rank: 1, value: 3750},
  6  : {name: 'Resistenza agli incantesimi (13)', rank: 1, bonus: 2},
  7  : {name: 'Scivolosa, migliorata', rank: 2, value: 15000},
  8  : {name: 'Ombra, migliorata', rank: 2, value: 15000},
  9  : {name: 'Movimento silenzioso, migliorato', rank: 2, value: 15000},
  10 : {name: 'Resistenza all\'acido', rank: 1, value: 18000},
  11 : {name: 'Resistenza al freddo', rank: 1, value: 18000},
  12 : {name: 'Resistenza all\'elettricità', rank: 1, value: 18000},
  13 : {name: 'Resistenza al fuoco', rank: 1, value: 18000},
  14 : {name: 'Resistenza al suono', rank: 1, value: 18000},
  15 : {name: 'Tocco fantasma', bonus: 3},
  16 : {name: 'Invulnerabilità', bonus: 3},
  17 : {name: 'Fortificazione, moderata', rank: 2, bonus: 3},
  18 : {name: 'Resistenza agli incantesimi (15)', rank: 2, bonus: 3},
  19 : {name: 'Selvatica', bonus: 3},
  20 : {name: 'Scivolosa, superiore', rank: 3, value: 33750},
  21 : {name: 'Ombra, superiore', rank: 3, value: 33750},
  22 : {name: 'Movimento silenzioso, superiore', rank: 3, value: 33750},
  23 : {name: 'Resistenza all\'acido, migliorata', rank: 2, value: 42000},
  24 : {name: 'Resistenza al freddo, migliorata', rank: 2, value: 42000},
  25 : {name: 'Resistenza all\'elettricità, migliorata', rank: 2, value: 42000},
  26 : {name: 'Resistenza al fuoco, migliorata', rank: 2, value: 42000},
  27 : {name: 'Resistenza al suono, migliorata', rank: 2, value: 42000},
  28 : {name: 'Resistenza agli incantesimi (17)', rank: 3, bonus: 4},
  29 : {name: 'Forma eterea', value: 49000},
  30 : {name: 'Controllo dei morti', value: 49000},
  31 : {name: 'Fortificazione, pesante', rank: 3, bonus: 5},
  32 : {name: 'Resistenza agli incantesimi (19)', rank: 4, bonus: 5},
  33 : {name: 'Resistenza all\'acido, superiore', rank: 3, value: 66000},
  34 : {name: 'Resistenza al freddo, superiore', rank: 3, value: 66000},
  35 : {name: 'Resistenza all\'elettricità, superiore', rank: 3, value: 66000},
  36 : {name: 'Resistenza al fuoco, superiore', rank: 3, value: 66000},
  37 : {name: 'Resistenza al suono, superiore', rank: 3, value: 66000},
  38 : {name: 'Reroll two times'}
}