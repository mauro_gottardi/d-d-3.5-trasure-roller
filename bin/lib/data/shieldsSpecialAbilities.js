exports.abilities = function(){
  return shieldsSpecialAbilities;
}

exports.ability = function(num){

  var number = num || false;

  if(number){
    return shieldsSpecialAbilities[number];
  } else {
    return shieldsSpecialAbilities;
  }

}

var shieldsSpecialAbilities = {
  1  : {name: 'Attirare frecce', bonus: 1},
  2  : {name: 'Sfondamento', bonus: 1},
  3  : {name: 'Accecante', bonus: 1},
  4  : {name: 'Fortificazione, leggera', bonus: 1},
  5  : {name: 'Deviazione delle frecce', bonus: 2},
  6  : {name: 'Animato', bonus: 2},
  7  : {name: 'Resistenza agli incantesimi (13)', bonus: 2},
  8  : {name: 'Resistenza all\'acido', rank: 1, value: 18000},
  9  : {name: 'Resistenza al freddo', rank: 1, value: 18000},
  10 : {name: 'Resistenza all\'elettricità', rank: 1, value: 18000},
  11 : {name: 'Resistenza al fuoco', rank: 1, value: 18000},
  12 : {name: 'Resistenza al suono', rank: 1, value: 18000},
  13 : {name: 'Tocco fantasma', bonus: 3},
  14 : {name: 'Fortificazione, moderata', bonus: 3},
  15 : {name: 'Resistenza agli incantesimi (15)', bonus: 3},
  16 : {name: 'Selvatico', bonus: 3},
  17 : {name: 'Resistenza all\'acido, migliorata', rank: 2, value: 42000},
  18 : {name: 'Resistenza al freddo, migliorata', rank: 2, value: 42000},
  19 : {name: 'Resistenza all\'elettricità, migliorata', rank: 2, value: 42000},
  20 : {name: 'Resistenza al fuoco, migliorata', rank: 2, value: 42000},
  21 : {name: 'Resistenza al suono, migliorata', rank: 2, value: 42000},
  22 : {name: 'Resistenza agli incantesimi (17)', bonus: 4},
  23 : {name: 'Controllo dei morti', value: 49000},
  24 : {name: 'Fortificazione, pesante', bonus: 5},
  25 : {name: 'Riflettente', bonus: 5},
  26 : {name: 'Resistenza agli incantesimi (19)', bonus: 5},
  27 : {name: 'Resistenza all\'acido, superiore', rank: 3, value: 66000},
  28 : {name: 'Resistenza al freddo, superiore', rank: 3, value: 66000},
  29 : {name: 'Resistenza all\'elettricità, superiore', rank: 3, value: 66000},
  30 : {name: 'Resistenza al fuoco, superiore', rank: 3, value: 66000},
  31 : {name: 'Resistenza al suono, superiore', rank: 3, value: 66000},
  32 : {name: 'Reroll two times'}
}