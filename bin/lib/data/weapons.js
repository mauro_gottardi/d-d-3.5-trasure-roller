exports.commonMelee = function(num){

  var number = num || false;

  if(number){
    return commonMelee[number];
  } else {
    return commonMelee;
  }

}

exports.uncommonMelee = function(num){

  var number = num || false;

  if(number){
    return uncommonMelee[number];
  } else {
    return uncommonMelee;
  }

}

exports.commonRanged = function(num){

  var number = num || false;

  if(number){
    return commonRanged[number];
  } else {
    return commonRanged;
  }

}

var commonMelee = {
  1 : 'Ascia da guerra nanica perfetta (330mo)',
  2 : 'Ascia bipenne perfetta (320mo)',
  3 : 'Bastone ferrato perfetto (600mo)',
  4 : 'Kama perfetto (302mo)',
  5 : 'Lancia corta perfetta (302mo)',
  6 : 'Mazza leggera perfetta (305mo)',
  7 : 'Mazza pesante perfetta (312mo)',
  8 : 'Nunchaku perfetto (302mo)',
  9 : 'Pugnale perfetto (302mo)',
  10 : 'Scimitarra perfetta (315mo)',
  11 : 'Siangham perfetto (303mo)',
  12 : 'Spada bastarda perfetta (335mo)',
  13 : 'Spada corta perfetta (310mo)',
  14 : 'Spada lunga perfetta (315mo)',
  15 : 'Spadone perfetto (315mo)',
  16 : 'Stocco perfetto (320mo)'
}

var uncommonMelee = {
  1 : 'Alabarda perfetta (310mo)',
  2 : 'Ascia perfetta (306mo)',
  3 : 'Ascia da battaglia perfetta (310mo)',
  4 : 'Balestra a mano perfetta (400mo)',
  5 : 'Balestra a ripetizione perfetta (550mo)',
  6 : 'Catena chiodata perfetta (325mo)',
  7 : 'Corsesca perfetta (310mo)',
  8 : 'Doppia ascia orchesca perfetta (660mo)',
  9 : 'Falce perfetta (318mo)',
  10 : 'Falcetto perfetto (306mo)',
  11 : 'Falchion perfetto (375mo)',
  12 : 'Falcione perfetto (308mo)',
  13 : 'Frusta perfetta (301mo)',
  14 : 'Giusarma perfetta (309mo)',
  15 : 'Guanto d\'arme perfetto (302mo)',
  16 : 'Guanto d\'arme chiodato perfetto (305mo)',
  17 : 'Kukri perfetto (308mo)',
  18 : 'Lancia da cavaliere perfetta (310mo)',
  19 : 'Lancia lunga perfetta (305mo)',
  20 : 'Manganello perfetto (301mo)',
  21 : 'Martello da guerra perfetto (312mo)',
  22 : 'Martello leggero perfetto (301mo)',
  23 : 'Martello-picca gnomesco perfetto (620mo)',
  24 : 'Mazzafrusto doppio perfetto (690mo)',
  25 : 'Mazzafrusto perfetto (308mo)',
  26 : 'Mazzafrusto pesante (315mo)',
  27 : 'Morning star perfetta (308mo)',
  28 : 'Piccone leggero perfetto (304mo)',
  29 : 'Piccone pesante perfetto (308mo)',
  30 : 'Pugnale da mischia perfetto (302mo)',
  31 : 'Randello perfetto (300mo)',
  32 : 'Randello pesante perfetto (305mo)',
  33 : 'Rete perfetta (320mo)',
  34 : 'Sai perfetto (301mo)',
  35 : 'Shuriken perfetto (301mo)',
  36 : 'Spada a due lame perfetta (700mo)',
  37 : 'Tridente perfetto (315mo)',
  38 : 'Urgrosh nanico perfetto (650mo)'
}

var commonRanged = {
  1 : 'Frecce (50)',
  2 : 'Quadrelli per balestra (50)',
  3 : 'Proiettili per fionda (50)',
  4 : 'Ascia da lancio perfetta (308mo)',
  5 : 'Balestra pesante perfetta (350mo)',
  6 : 'Balestra leggera perfetta (335mo)',
  7 : 'Dardi perfetti (300mo)',
  8 : 'Giavellotto perfetto (301mo)',
  9 : 'Arco corto perfetto (330mo)',
  10 : 'Arco corto composito perfetto (bonus For +0)(375mo)',
  11 : 'Arco corto composito perfetto (bonus For +1)(450mo)',
  12 : 'Arco corto composito perfetto (bonus For +2)(525mo)',
  13 : 'Fionda perfetta (300mo)',
  14 : 'Arco lungo perfetto (375mo)',
  15 : 'Arco lungo composito perfetto (400mo)',
  16 : 'Arco lungo composito perfetto (bonus For +1)(500mo)',
  17 : 'Arco lungo composito perfetto (bonus For +2)(600mo)',
  18 : 'Arco lungo composito perfetto (bonus For +3)(700mo)',
  19 : 'Arco lungo composito perfetto (bonus For +4)(800mo)'
}