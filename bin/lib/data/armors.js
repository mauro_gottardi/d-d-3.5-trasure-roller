exports.item = function(num){

  var number = num || false;

  if(number){
    return armors[number];
  } else {
    return armors;
  }

}

var armors = {
  1 : 'Giacco di maglia (100mo)',
  2 : 'Corazza perfetta di cuoio borchiato (175mo)',
  3 : 'Corazza di piastre (200mo)',
  4 : 'Corazza di bande (250mo)',
  5 : 'Mezza armatura (600mo)',
  6 : 'Armatura completa (1500mo)',
  7 : 'Buckler in legnoscuro (205mo)',
  8 : 'Scudo in legnoscuro (205mo)',
  9 : 'Buckler perfetto (165mo)',
  10 : 'Scudo leggero di legno perfetto (153mo)',
  11 : 'Scudo leggero di metallo perfetto (159mo)',
  12 : 'Scudo pesante di legno perfetto (157mo)',
  13 : 'Scudo pesante di metallo perfetto (170mo)'
}