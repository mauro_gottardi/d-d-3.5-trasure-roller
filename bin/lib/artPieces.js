var utils = require('./utilities');

exports.all = function(){
  return artPieces;
}

exports.byRarity = function(number){
  return artPieces[number];
}

exports.getRandomArtPiece = function(){

  var rarity = utils.getArtRarity();

  var artPieceJson = artPieces[rarity];
  var value = utils.calculateValue(artPieceJson.value);
  var name = utils.findName(artPieceJson.names);

  var artPiece = {
    name    : name,
    dice    : artPieceJson.valueStr,
    value   : value,
    avarage : artPieceJson.avarage
  }

  return artPiece;

}

exports.getSpecificNumberOfArtPieces = function(number){

  var array = [];

  for(i=0; i<number; i++){
    array.push(this.getRandomArtPiece());
  }

  return array;

}


var artPieces = {
  1 : {
    range   : '1-10',
    avarage : '55 mo',
    valueStr: '1d10x10 mo',
    value   : '1-10-10',
    names   : ['Brocca d\'argento', 'Statuetta d\'avorio', 'Statuetta d\'osso intagliato', 'Braccialetto d\'oro finemente lavorato']
  },
  2 : {
    range   : '11-25',
    avarage : '105 mo',
    valueStr: '3d6x10 mo',
    value   : '3-6-10',
    names   : ['Abiti d\'oro', 'Maschera di velluto nero con numerosi quarzi citrini', 'Calice d\'argento con lapislazzuli']
  },
  3 : {
    range   : '26-40',
    avarage : '350 mo',
    valueStr: '1d6x100 mo',
    value   : '1-6-100',
    names   : ['Grande arazzo pregiato di lana', 'Boccale di ottone con intarsi in giada']
  },
  4 : {
    range   : '41-50',
    avarage : '550 mo',
    valueStr: '1d10x100 mo',
    value   : '1-10-100',
    names   : ['Pettine d\'argento con seleniti', 'Spada lunga d\'acciaio placcata in argento e con pietra di gialetto nell\'elsa']
  },
  5 : {
    range   : '51-60',
    avarage : '700 mo',
    valueStr: '2d6x100 mo',
    value   : '2-6-100',
    names   : ['Arpa in legno esotico con intarsi in avorio e zirconi', 'Idolo d\'oro massiccio (5kg)']
  },
  6 : {
    range   : '61-70',
    avarage : '1050 mo',
    valueStr: '3d6x100 mo',
    value   : '3-6-100',
    names   : ['Pettine d\'oro a forma di drago con occhi di granato rosso', 'Tappo di bottiglia in oro e topazio', 'Pugnale da cerimonia in electrum con rubino a stella sul pomello']
  },
  7 : {
    range   : '71-80',
    avarage : '1400 mo',
    valueStr: '4d6x100 mo',
    value   : '4-6-100',
    names   : ['Benda per occhio con finto occhio in zaffiro e selenite', 'Pregiata catena d\'oro con pendente in opale di fuoco', 'Antico dipinto d\'ottima fattura']
  },
  8 : {
    range   : '81-85',
    avarage : '1750 mo',
    valueStr: '5d6x100 mo',
    value   : '5-6-100',
    names   : ['Mantello in velluto e seta ricamato con numerose seleniti', 'Catenda d\'oro con pendente in zaffiro']
  },
  9 : {
    range   : '86-90',
    avarage : '2500 mo',
    valueStr: '1d4x1000 mo',
    value   : '1-4-1000',
    names   : ['Guanto ricamato con gemme', 'Cavigliera decorata con pietre preziose', 'Carillon d\'oro']
  },
  10 : {
    range   : '91-95',
    avarage : '3500 mo',
    valueStr: '1d6x1000 mo',
    value   : '1-6-1000',
    names   : ['Diadema in oro con quattro acquamarine', 'Fila di perline rosa (collana)']
  },
  11 : {
    range   : '96-99',
    avarage : '5000 mo',
    valueStr: '2d4x1000 mo',
    value   : '2-4-1000',
    names   : ['Corona d\'oro con gemme', 'Anelle in electrum con pietre preziose']
  },
  12 : {
    range   : '100-100',
    avarage : '7000 mo',
    valueStr: '2d6x1000 mo',
    value   : '2-6-1000',
    names   : ['Anello in oro e rubini', 'Servizio di tazze in oro e smeraldi']
  }
}