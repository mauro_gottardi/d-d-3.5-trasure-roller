var utils = require('../utilities');

exports.all = function(){
  return specificArmors;
}

exports.getRandomSpecificArmor = function(rarity){

  var rarity = rarity || 'minor';
  var num = utils.getRandomNumber();
  var armor;
 
  if(rarity = 'minor'){
    armor = specificArmors[minor[num]];
  } else if (rarity = 'medium'){
    armor = specificArmors[medium[num]];
  } else if (rarity = 'major'){
    armor = specificArmors[major[num]];
  } else {
    throw new Error ("rarity must be 'minor', 'medium' or 'major'");
  }

  return armor

}

var specificArmors = {
  1 : {name: 'Giacco di maglia in mithral', value: 1100},
  2 : {name: 'Armatura di scaglie di drago', value: 3300},
  3 : {name: 'Cotta di maglia elfica', value: 4150},
  4 : {name: 'Pelle di rinocerone', value: 5165},
  5 : {name: 'Corazza di piastre in adamantio', value: 10200},
  6 : {name: 'Armatura completa nanica', value: 16500},
  7 : {name: 'Corazza di bande della fortuna', value: 18900},
  8 : {name: 'Armatura celestiale', value: 22400},
  9 : {name: 'Armatura completa delle profondità', value: 24650},
  10 : {name: 'Corazza di piastre del comando', value: 25400},
  11 : {name: 'Armatura in mithral della velocità', value: 26500},
  12 : {name: 'Armatura demoniaca', value: 52260}
}

var minor = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1,
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 1, 17 : 1, 18 : 1, 19 : 1, 20  : 1, 
  21 : 1, 22 : 1, 23 : 1, 24 : 1, 25 : 1, 26 : 1, 27 : 1, 28 : 1, 29 : 1, 30  : 1, 
  31 : 1, 32 : 1, 33 : 1, 34 : 1, 35 : 1, 36 : 1, 37 : 1, 38 : 1, 39 : 1, 40  : 1, 
  41 : 1, 42 : 1, 43 : 1, 44 : 1, 45 : 1, 46 : 1, 47 : 1, 48 : 1, 49 : 1, 50  : 1, 
  51 : 2, 52 : 2, 53 : 2, 54 : 2, 55 : 2, 56 : 2, 57 : 2, 58 : 2, 59 : 2, 60  : 2, 
  61 : 2, 62 : 2, 63 : 2, 64 : 2, 65 : 2, 66 : 2, 67 : 2, 68 : 2, 69 : 2, 70  : 2, 
  71 : 2, 72 : 2, 73 : 2, 74 : 2, 75 : 2, 76 : 2, 77 : 2, 78 : 2, 79 : 2, 80  : 2, 
  81 : 3, 82 : 3, 83 : 3, 84 : 3, 85 : 3, 86 : 3, 87 : 3, 88 : 3, 89 : 3, 90  : 3, 
  91 : 3, 92 : 3, 93 : 3, 94 : 3, 95 : 3, 96 : 3, 97 : 3, 98 : 3, 99 : 3, 100 : 3 
}

var medium = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1,
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 1, 17 : 1, 18 : 1, 19 : 1, 20  : 1, 
  21 : 1, 22 : 1, 23 : 1, 24 : 1, 25 : 1, 26 : 2, 27 : 2, 28 : 2, 29 : 2, 30  : 2, 
  31 : 2, 32 : 2, 33 : 2, 34 : 2, 35 : 2, 36 : 2, 37 : 2, 38 : 2, 39 : 2, 40  : 2, 
  41 : 2, 42 : 2, 43 : 2, 44 : 2, 45 : 2, 46 : 3, 47 : 3, 48 : 3, 49 : 3, 50  : 3, 
  51 : 3, 52 : 3, 53 : 3, 54 : 3, 55 : 3, 56 : 3, 57 : 3, 58 : 4, 59 : 4, 60  : 4, 
  61 : 4, 62 : 4, 63 : 4, 64 : 4, 65 : 4, 66 : 4, 67 : 4, 68 : 5, 69 : 5, 70  : 5, 
  71 : 5, 72 : 5, 73 : 5, 74 : 5, 75 : 5, 76 : 5, 77 : 5, 78 : 5, 79 : 5, 80  : 5, 
  81 : 5, 82 : 5, 83 : 6, 84 : 6, 85 : 6, 86 : 6, 87 : 6, 88 : 6, 89 : 6, 90  : 6, 
  91 : 6, 92 : 6, 93 : 6, 94 : 6, 95 : 6, 96 : 6, 97 : 6, 98 : 7, 99 : 7, 100 : 7 
}

var major = {
  1  : 5,  2  : 5,  3  : 5,  4  : 5,  5  : 5,  6  : 5,  7  : 5,  8  : 5,  9  : 5,  10  : 5,
  11 : 6,  12 : 6,  13 : 6,  14 : 6,  15 : 6,  16 : 6,  17 : 6,  18 : 6,  19 : 6,  20  : 6, 
  21 : 7,  22 : 7,  23 : 7,  24 : 7,  25 : 7,  26 : 7,  27 : 7,  28 : 7,  29 : 7,  30  : 7, 
  31 : 7,  32 : 7,  33 : 8,  34 : 8,  35 : 8,  36 : 8,  37 : 8,  38 : 8,  39 : 8,  40  : 8, 
  41 : 8,  42 : 8,  43 : 8,  44 : 8,  45 : 8,  46 : 8,  47 : 8,  48 : 8,  49 : 8,  50  : 8, 
  51 : 9,  52 : 9,  53 : 9,  54 : 9,  55 : 9,  56 : 9,  57 : 9,  58 : 9,  59 : 9,  60  : 9, 
  61 : 10, 62 : 10, 63 : 10, 64 : 10, 65 : 10, 66 : 10, 67 : 10, 68 : 10, 69 : 10, 70  : 10, 
  71 : 10, 72 : 10, 73 : 10, 74 : 10, 75 : 10, 76 : 11, 77 : 11, 78 : 11, 79 : 11, 80  : 11, 
  81 : 11, 82 : 11, 83 : 11, 84 : 11, 85 : 11, 86 : 11, 87 : 11, 88 : 11, 89 : 11, 90  : 11, 
  91 : 12, 92 : 12, 93 : 12, 94 : 12, 95 : 12, 96 : 12, 97 : 12, 98 : 12, 99 : 12, 100 : 12 
}
