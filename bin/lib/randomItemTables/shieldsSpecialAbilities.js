var utils = require('../utilities');
var shieldsSpecialAbilities = require('../data/shieldsSpecialAbilities');

exports.all = function(){
  return shieldsSpecialAbilities.ability();
}

exports.getRandomAbilities = function(rarity, currentBonus){

  var num = utils.getRandomNumber();
  var abilitiesIds = [];

  num = 100;

  if(num !== 100) {
    abilitiesIds.push(num);
  } else {
    abilitiesIds = utils.getAbilities();
  }

  var ability;
  var abilities = [];
  var abilityNumber;
  var abilitiesNumber = [];

  // get the number of the abilities

  abilitiesIds.forEach(function(id){
    abilityNumber = (rarity === 'minor') ? minor[id] : abilityNumber;
    abilityNumber = (rarity === 'medium') ? medium[id] : abilityNumber;
    abilityNumber = (rarity === 'major') ? major[id] : abilityNumber;

    if (abilitiesNumber.indexOf(abilityNumber) === -1){
      abilitiesNumber.push(abilityNumber);  
    }

  })

  // select the abilities

  abilitiesNumber.forEach(function(number){
    abilities.push(shieldsSpecialAbilities.ability(number)); 
  })

  // keep the one with higher rank

  // TODO

  return abilities

}

var bonusPrices = {
  6  : 36000,
  7  : 49000,
  8  : 64000,
  9  : 81000,
  10 : 100000
}

var minor = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1,
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 1, 17 : 1, 18 : 1, 19 : 1, 20  : 1, 
  21 : 2, 22 : 2, 23 : 2, 24 : 2, 25 : 2, 26 : 2, 27 : 2, 28 : 2, 29 : 2, 30  : 2, 
  31 : 2, 32 : 2, 33 : 2, 34 : 2, 35 : 2, 36 : 2, 37 : 2, 38 : 2, 39 : 2, 40  : 2, 
  41 : 3, 42 : 3, 43 : 3, 44 : 3, 45 : 3, 46 : 3, 47 : 3, 48 : 3, 49 : 3, 50  : 3, 
  51 : 4, 52 : 4, 53 : 4, 54 : 4, 55 : 4, 56 : 4, 57 : 4, 58 : 4, 59 : 4, 60  : 4, 
  61 : 4, 62 : 4, 63 : 4, 64 : 4, 65 : 4, 66 : 4, 67 : 4, 68 : 4, 69 : 4, 70  : 4, 
  71 : 4, 72 : 4, 73 : 4, 74 : 4, 75 : 4, 76 : 5, 77 : 5, 78 : 5, 79 : 5, 80  : 5, 
  81 : 5, 82 : 5, 83 : 5, 84 : 5, 85 : 5, 86 : 5, 87 : 5, 88 : 5, 89 : 5, 90  : 5, 
  91 : 5, 92 : 5, 93 : 6, 94 : 6, 95 : 6, 96 : 6, 97 : 6, 98 : 7, 99 : 7, 100 : 32 
}

var medium = {
  1  : 1,  2  : 1,  3  : 1,  4  : 1,  5  : 1,  6  : 1,  7  : 1,  8  : 1,  9  : 1,  10  : 1,
  11 : 2,  12 : 2,  13 : 2,  14 : 2,  15 : 2,  16 : 2,  17 : 2,  18 : 2,  19 : 2,  20  : 2, 
  21 : 3,  22 : 3,  23 : 3,  24 : 3,  25 : 3,  26 : 4,  27 : 4,  28 : 4,  29 : 4,  30  : 4, 
  31 : 4,  32 : 4,  33 : 4,  34 : 4,  35 : 4,  36 : 4,  37 : 4,  38 : 4,  39 : 4,  40  : 4, 
  41 : 5,  42 : 5,  43 : 5,  44 : 5,  45 : 5,  46 : 5,  47 : 5,  48 : 5,  49 : 5,  50  : 5, 
  51 : 6,  52 : 6,  53 : 6,  54 : 6,  55 : 6,  56 : 6,  57 : 6,  58 : 7,  59 : 7,  60  : 8, 
  61 : 8,  62 : 8,  63 : 8,  64 : 9,  65 : 9,  66 : 9,  67 : 9,  68 : 10, 69 : 10, 70  : 10, 
  71 : 10, 72 : 11, 73 : 11, 74 : 11, 75 : 11, 76 : 12, 77 : 12, 78 : 12, 79 : 12, 80  : 13, 
  81 : 13, 82 : 13, 83 : 13, 84 : 13, 85 : 13, 86 : 14, 87 : 14, 88 : 14, 89 : 14, 90  : 14, 
  91 : 14, 92 : 14, 93 : 14, 94 : 14, 95 : 14, 96 : 15, 97 : 15, 98 : 15, 99 : 16, 100 : 32 
}

var major = {
  1  : 1,  2  : 1,  3  : 1,  4  : 1,  5  : 1,  6  : 2,  7  : 2,  8  : 2,  9  : 3,  10  : 3,
  11 : 4,  12 : 4,  13 : 4,  14 : 4,  15 : 4,  16 : 5,  17 : 5,  18 : 5,  19 : 5,  20  : 5, 
  21 : 6,  22 : 6,  23 : 6,  24 : 6,  25 : 6,  26 : 8,  27 : 8,  28 : 8,  29 : 9,  30  : 9, 
  31 : 9,  32 : 10, 33 : 10, 34 : 10, 35 : 11, 36 : 11, 37 : 11, 38 : 12, 39 : 12, 40  : 12, 
  41 : 13, 42 : 13, 43 : 13, 44 : 13, 45 : 13, 46 : 13, 47 : 14, 48 : 14, 49 : 14, 50  : 14, 
  51 : 14, 52 : 14, 53 : 14, 54 : 14, 55 : 14, 56 : 14, 57 : 15, 58 : 15, 59 : 16, 60  : 17, 
  61 : 17, 62 : 17, 63 : 17, 64 : 17, 65 : 18, 66 : 18, 67 : 18, 68 : 18, 69 : 18, 70  : 19, 
  71 : 19, 72 : 19, 73 : 19, 74 : 19, 75 : 20, 76 : 20, 77 : 20, 78 : 20, 79 : 20, 80  : 21, 
  81 : 21, 82 : 21, 83 : 21, 84 : 21, 85 : 22, 86 : 22, 87 : 23, 88 : 24, 89 : 24, 90  : 24, 
  91 : 24, 92 : 25, 93 : 25, 94 : 26, 95 : 27, 96 : 28, 97 : 29, 98 : 30, 99 : 31, 100 : 32 
}
