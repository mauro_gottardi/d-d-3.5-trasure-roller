var utils = require('../utilities');
var armorsSpecialAbilities = require('../data/armorsSpecialAbilities');

exports.all = function(){
  return armorsSpecialAbilities.ability();
}

exports.getRandomAbilities = function(rarity, currentBonus){

  var num = utils.getRandomNumber();
  var abilitiesIds = [];

  num = 100;

  if(num !== 100) {
    abilitiesIds.push(num);
  } else {
    abilitiesIds = utils.getAbilities();
  }

  var ability;
  var abilities = [];
  var abilityNumber;
  var abilitiesNumber = [];

  // get the number of the abilities

  abilitiesIds.forEach(function(id){
    abilityNumber = (rarity === 'minor') ? minor[id] : abilityNumber;
    abilityNumber = (rarity === 'medium') ? medium[id] : abilityNumber;
    abilityNumber = (rarity === 'major') ? major[id] : abilityNumber;

    if (abilitiesNumber.indexOf(abilityNumber) === -1){
      abilitiesNumber.push(abilityNumber);  
    }

  })

  // select the abilities

  abilitiesNumber.forEach(function(number){
    abilities.push(armorsSpecialAbilities.ability(number)); 
  })

  // keep the one with higher rank

  // TODO

  return abilities

}

var bonusPrices = {
  6  : 36000,
  7  : 49000,
  8  : 64000,
  9  : 81000,
  10 : 100000
}

var minor = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1, 
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 1, 17 : 1, 18 : 1, 19 : 1, 20  : 1, 
  21 : 1, 22 : 1, 23 : 1, 24 : 1, 25 : 1, 26 : 2, 27 : 2, 28 : 2, 29 : 2, 30  : 2, 
  31 : 2, 32 : 2, 33 : 3, 34 : 3, 35 : 3, 36 : 3, 37 : 3, 38 : 3, 39 : 3, 40  : 3, 
  41 : 3, 42 : 3, 43 : 3, 44 : 3, 45 : 3, 46 : 3, 47 : 3, 48 : 3, 49 : 3, 50  : 3, 
  51 : 3, 52 : 3, 53 : 4, 54 : 4, 55 : 4, 56 : 4, 57 : 4, 58 : 4, 59 : 4, 60  : 4, 
  61 : 4, 62 : 4, 63 : 4, 64 : 4, 65 : 4, 66 : 4, 67 : 4, 68 : 4, 69 : 4, 70  : 4, 
  71 : 4, 72 : 4, 73 : 5, 74 : 5, 75 : 5, 76 : 5, 77 : 5, 78 : 5, 79 : 5, 80  : 5, 
  81 : 5, 82 : 5, 83 : 5, 84 : 5, 85 : 5, 86 : 5, 87 : 5, 88 : 5, 89 : 5, 90  : 5, 
  91 : 5, 92 : 5, 93 : 6, 94 : 6, 95 : 6, 96 : 6, 97 : 7, 98 : 8, 99 : 9, 100 : 38
}

var medium = {
  1  : 1,  2  : 1,  3  : 1,  4  : 1,  5  : 1,  6  : 2,  7  : 2,  8  : 2,  9  : 3,  10  : 3,  
  11 : 3,  12 : 4,  13 : 4,  14 : 4,  15 : 5,  16 : 5,  17 : 5,  18 : 6,  19 : 6,  20  : 7,  
  21 : 7,  22 : 7,  23 : 7,  24 : 7,  25 : 7,  26 : 7,  27 : 7,  28 : 7,  29 : 7,  30  : 8,  
  31 : 8,  32 : 8,  33 : 8,  34 : 8,  35 : 8,  36 : 8,  37 : 8,  38 : 8,  39 : 8,  40  : 9,
  41 : 9,  42 : 9,  43 : 9,  44 : 9,  45 : 9,  46 : 9,  47 : 9,  48 : 9,  49 : 9,  50  : 10, 
  51 : 10, 52 : 10, 53 : 10, 54 : 10, 55 : 11, 56 : 11, 57 : 11, 58 : 11, 59 : 11, 60  : 12,
  61 : 12, 62 : 12, 63 : 12, 64 : 12, 65 : 13, 66 : 13, 67 : 13, 68 : 13, 69 : 13, 70  : 14, 
  71 : 14, 72 : 14, 73 : 14, 74 : 14, 75 : 15, 76 : 15, 77 : 15, 78 : 15, 79 : 15, 80  : 16, 
  81 : 16, 82 : 16, 83 : 16, 84 : 16, 85 : 17, 86 : 17, 87 : 17, 88 : 17, 89 : 17, 90  : 18, 
  91 : 18, 92 : 18, 93 : 18, 94 : 18, 95 : 19, 96 : 19, 97 : 19, 98 : 19, 99 : 19, 100 : 38
}

var major = {
  1  : 1,  2  : 1,  3  : 1,  4  : 2,  5  : 7,  6  : 7,  7  : 7,  8  : 8,  9  : 8,  10  : 8,  
  11 : 9,  12 : 9,  13 : 9,  14 : 10, 15 : 10, 16 : 10, 17 : 11, 18 : 11, 19 : 11, 20  : 12, 
  21 : 12, 22 : 12, 23 : 13, 24 : 13, 25 : 13, 26 : 14, 27 : 14, 28 : 14, 29 : 15, 30  : 15, 
  31 : 15, 32 : 15, 33 : 15, 34 : 16, 35 : 16, 36 : 17, 37 : 17, 38 : 17, 39 : 17, 40  : 17, 
  41 : 18, 42 : 18, 43 : 19, 44 : 20, 45 : 20, 46 : 20, 47 : 20, 48 : 20, 49 : 21, 50  : 21, 
  51 : 21, 52 : 21, 53 : 21, 54 : 22, 55 : 22, 56 : 22, 57 : 22, 58 : 22, 59 : 23, 60  : 23, 
  61 : 23, 62 : 23, 63 : 23, 64 : 24, 65 : 24, 66 : 24, 67 : 24, 68 : 24, 69 : 25, 70  : 25, 
  71 : 25, 72 : 25, 73 : 25, 74 : 26, 75 : 26, 76 : 26, 77 : 26, 78 : 26, 79 : 27, 80  : 27, 
  81 : 27, 82 : 27, 83 : 27, 84 : 28, 85 : 28, 86 : 28, 87 : 28, 88 : 28, 89 : 29, 90  : 30, 
  91 : 31, 92 : 31, 93 : 32, 94 : 32, 95 : 33, 96 : 34, 97 : 35, 98 : 36, 99 : 37, 100 : 38
}
