var utils = require('../utilities');

exports.all = function(){
  return specificArmors;
}

exports.getRandomSpecificShield = function(rarity){

  var rarity = rarity || 'minor';
  var num = utils.getRandomNumber();
  var shield;
 
  if(rarity = 'minor'){
    shield = specificSchields[minor[num]];
  } else if (rarity = 'medium'){
    shield = specificSchields[medium[num]];
  } else if (rarity = 'major'){
    shield = specificSchields[major[num]];
  } else {
    throw new Error ("rarity must be 'minor', 'medium' or 'major'");
  }

  return shield

}

var specificSchields = {
  1 : {name: 'Buckler di legnoscuro', value: 205},
  2 : {name: 'Scudo in legnoscuro', value: 257},
  3 : {name: 'Scudo pesante in mithral', value: 1020},
  4 : {name: 'Scudo dell\'incantatore', value: 3153},
  5 : {name: 'Scudo di spine', value: 5580},
  6 : {name: 'Scudo del leone', value: 9170},
  7 : {name: 'Scudo alato', value: 17257},
  8 : {name: 'Scudo assorbente', value: 50170}
}

var minor = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1,
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 1, 17 : 1, 18 : 1, 19 : 1, 20  : 1, 
  21 : 1, 22 : 1, 23 : 1, 24 : 1, 25 : 1, 26 : 1, 27 : 1, 28 : 1, 29 : 1, 30  : 1, 
  31 : 2, 32 : 2, 33 : 2, 34 : 2, 35 : 2, 36 : 2, 37 : 2, 38 : 2, 39 : 2, 40  : 2, 
  41 : 2, 42 : 2, 43 : 2, 44 : 2, 45 : 2, 46 : 2, 47 : 2, 48 : 2, 49 : 2, 50  : 2, 
  51 : 2, 52 : 2, 53 : 2, 54 : 2, 55 : 2, 56 : 2, 57 : 2, 58 : 2, 59 : 2, 60  : 2, 
  61 : 2, 62 : 2, 63 : 2, 64 : 2, 65 : 2, 66 : 2, 67 : 2, 68 : 2, 69 : 2, 70  : 2, 
  71 : 2, 72 : 2, 73 : 2, 74 : 2, 75 : 2, 76 : 2, 77 : 2, 78 : 2, 79 : 2, 80  : 2, 
  81 : 3, 82 : 3, 83 : 3, 84 : 3, 85 : 3, 86 : 3, 87 : 3, 88 : 3, 89 : 3, 90  : 3, 
  91 : 3, 92 : 3, 93 : 3, 94 : 3, 95 : 3, 96 : 4, 97 : 4, 98 : 4, 99 : 4, 100 : 4 
}

var medium = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1,
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 1, 17 : 1, 18 : 1, 19 : 1, 20  : 1, 
  21 : 2, 22 : 2, 23 : 2, 24 : 2, 25 : 2, 26 : 2, 27 : 2, 28 : 2, 29 : 2, 30  : 2, 
  31 : 2, 32 : 2, 33 : 2, 34 : 2, 35 : 2, 36 : 2, 37 : 2, 38 : 2, 39 : 2, 40  : 2, 
  41 : 2, 42 : 2, 43 : 2, 44 : 2, 45 : 2, 46 : 3, 47 : 3, 48 : 3, 49 : 3, 50  : 3, 
  51 : 3, 52 : 3, 53 : 3, 54 : 3, 55 : 3, 56 : 3, 57 : 3, 58 : 3, 59 : 3, 60  : 3, 
  61 : 3, 62 : 3, 63 : 3, 64 : 3, 65 : 3, 66 : 3, 67 : 3, 68 : 3, 69 : 3, 70  : 3, 
  71 : 4, 72 : 4, 73 : 4, 74 : 4, 75 : 4, 76 : 4, 77 : 4, 78 : 4, 79 : 4, 80  : 4, 
  81 : 4, 82 : 4, 83 : 4, 84 : 4, 85 : 4, 86 : 5, 87 : 5, 88 : 5, 89 : 5, 90  : 5, 
  91 : 6, 92 : 6, 93 : 6, 94 : 6, 95 : 6, 96 : 7, 97 : 7, 98 : 7, 99 : 7, 100 : 7 
}

var major = {
  1  : 4, 2  : 4, 3  : 4, 4  : 4, 5  : 4, 6  : 4, 7  : 4, 8  : 4, 9  : 4, 10  : 4,
  11 : 4, 12 : 4, 13 : 4, 14 : 4, 15 : 4, 16 : 4, 17 : 4, 18 : 4, 19 : 4, 20  : 4, 
  21 : 5, 22 : 5, 23 : 5, 24 : 5, 25 : 5, 26 : 5, 27 : 5, 28 : 5, 29 : 5, 30  : 5, 
  31 : 5, 32 : 5, 33 : 5, 34 : 5, 35 : 5, 36 : 5, 37 : 5, 38 : 5, 39 : 5, 40  : 5, 
  41 : 6, 42 : 6, 43 : 6, 44 : 6, 45 : 6, 46 : 6, 47 : 6, 48 : 6, 49 : 6, 50  : 6, 
  51 : 6, 52 : 6, 53 : 6, 54 : 6, 55 : 6, 56 : 6, 57 : 6, 58 : 6, 59 : 6, 60  : 6, 
  61 : 7, 62 : 7, 63 : 7, 64 : 7, 65 : 7, 66 : 7, 67 : 7, 68 : 7, 69 : 7, 70  : 7, 
  71 : 7, 72 : 7, 73 : 7, 74 : 7, 75 : 7, 76 : 7, 77 : 7, 78 : 7, 79 : 7, 80  : 7, 
  81 : 7, 82 : 7, 83 : 7, 84 : 7, 85 : 7, 86 : 7, 87 : 7, 88 : 7, 89 : 7, 90  : 7, 
  91 : 8, 92 : 8, 93 : 8, 94 : 8, 95 : 8, 96 : 8, 97 : 8, 98 : 8, 99 : 8, 100 : 8 
}
