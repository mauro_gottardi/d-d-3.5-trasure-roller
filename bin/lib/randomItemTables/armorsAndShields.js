var utils = require('../utilities');
var specificArmors = require('./specificArmors');
var specificShields = require('./specificShields');
var armorAbilities = require('./armorsSpecialAbilities');
var shieldAbilities = require('./shieldsSpecialAbilities');

exports.allSchields = function(){
  return shieldKinds;
}

exports.allArmors = function(){
  return armorKinds;
}

exports.all = function(){
  return generalArmorsAndShields;
}

exports.generalArmorsAndShields = function(){
  return generalArmorsAndShields;
}

exports.getRandomArmorOrShield = function(rarity){

  var rndNum = utils.getRandomNumber();
  var item = {};

  if(rarity === 'minor'){

    var support = generalArmorsAndShields[major[rndNum]];
    item.type = support.type;
    item.bonus = support.bonus;
    item.value = support.value;

  } else if (rarity === 'medium'){

    var support = generalArmorsAndShields[major[rndNum]];
    item.type = support.type;
    item.bonus = support.bonus;
    item.value = support.value;

  } else if (rarity === 'major'){

    var support = generalArmorsAndShields[major[rndNum]];
    item.type = support.type;
    item.bonus = support.bonus;
    item.value = support.value;

  }

  console.log(item);

  // find right material
  if (utils.getRandomNumber() <= 95){
    item.material = 'Normale';
  } else {
    item.material = specialMaterials[utils.getRandomNumber(6)];
  }

  
  // get item 

  if(item.type === 'shield'){
   
    var n = utils.getRandomNumber();
    var shield = shieldKinds[randomShield[n]]
    item.name = shield.name;
    item.value = item.value + shield.value;
  
  } else if (item.type === 'armor'){

    var n = utils.getRandomNumber();
    var armor = armorKinds[randomArmor[n]]
    item.name = armor.name;
    item.value = item.value + armor.value;

  } else if (item.type === 'specific armor'){

    item = null;
    item = specificArmors.getRandomSpecificArmor(rarity);

  } else if (item.type === 'specific shield'){

    item = null;
    item = specificShields.getRandomSpecificShield(rarity);

  } else if (item.type === 'special ability and reroll'){

    // get special ability
    var numAbilities = 1;
    var rerolledItem = [];
    var infoObj = null;
    infoObj = rerollItem(rarity, numAbilities, rerolledItem);

    console.log('infoObj');
    console.log(infoObj);

    if (infoObj.rerolledItem.type === 'specific armor'){

      item = null;
      item = specificArmors.getRandomSpecificArmor(rarity);

    } else if (infoObj.rerolledItem.type === 'specific shield'){

      item = null;
      item = specificShields.getRandomSpecificShield(rarity);

    } else {

      var abilities = [];
      var currentAbility;
      var i = 0;
      
      for (i=0; i<infoObj.numAbilities;i++){


        currentAbility = (infoObj.rerolledItem.type === 'armor') ? 
          armorAbilities.getRandomAbilities(rarity, infoObj.rerolledItem.bonus) :
          shieldAbilities.getRandomAbilities(rarity, infoObj.rerolledItem.bonus);

        abilities.push(currentAbility);
      
      }

      item.abilities = abilities;

      return {num: rndNum, item: item, rerolledItem: rerolledItem};

    }

  }

  return {num: rndNum, item: item, rerolledItem: null};

}

function rerollItem(rarity, numAbilities, rerolledItem){

  // roll another item
  var rndNumReroll = utils.getRandomNumber();
  var itemReroll = {};

  if(rarity === 'minor'){
    
    var support = generalArmorsAndShields[major[rndNumReroll]];
    itemReroll.type = support.type;
    itemReroll.bonus = support.bonus;
    itemReroll.value = support.value;

  } else if (rarity === 'medium'){

    var support = generalArmorsAndShields[major[rndNumReroll]];
    itemReroll.type = support.type;
    itemReroll.bonus = support.bonus;
    itemReroll.value = support.value;

  } else if (rarity === 'major'){

    var support = generalArmorsAndShields[major[rndNumReroll]];
    itemReroll.type = support.type;
    itemReroll.bonus = support.bonus;
    itemReroll.value = support.value;

  }

  if (itemReroll.type === 'special ability and reroll'){
    numAbilities += 1;
    rerollItem(rarity, numAbilities, rerolledItem);
  } else {
    rerolledItem.push(itemReroll);
    return {rerolledItem: rerolledItem, numAbilities: numAbilities};
  }

  return {rerolledItem: rerolledItem, numAbilities: numAbilities};

}


var generalArmorsAndShields = {
  1 : {type: 'shield', bonus: 1, value: 1000},
  2 : {type: 'armor', bonus: 1, value: 1000},
  3 : {type: 'shield', bonus: 2, value: 4000},
  4 : {type: 'armor', bonus: 2, value: 4000},
  5 : {type: 'shield', bonus: 3, value: 9000},
  6 : {type: 'armor', bonus: 3, value: 9000},
  7 : {type: 'shield', bonus: 4, value: 16000},
  8 : {type: 'armor', bonus: 4, value: 16000},
  9 : {type: 'shield', bonus: 5, value: 25000},
  10 : {type: 'armor', bonus: 6, value: 25000},
  11 : {type: 'specific armor', bonus: null, value: null},
  12 : {type: 'specific shield', bonus: null, value: null},
  13 : {type: 'special ability and reroll', bonus: null, value: null}
}

var armorKinds = {
  1 : {name: 'Imbottita', value: 155},
  2 : {name: 'Cuoio', value: 160},
  3 : {name: 'Cuoio borchiato', value: 175},
  4 : {name: 'Giaco di maglia', value: 250},
  5 : {name: 'Pelle', value: 165},
  6 : {name: 'Corazza a scaglie', value: 200},
  7 : {name: 'Cotta di maglia', value: 300},
  8 : {name: 'Corazza di piastre', value: 350},
  9 : {name: 'Corazza a strisce', value: 350},
  10 : {name: 'Corazza di bande', value: 400},
  11 : {name: 'Mezza armatura', value: 750},
  12 : {name: 'Armatura completa', value: 1650}
}

var randomArmor = {
  1  : 1,  2  : 2,  3  : 3,  4  : 3,  5  : 3,  6  : 3,  7  : 3,  8  : 3,  9  : 3,  10  : 3,
  11 : 3,  12 : 3,  13 : 3,  14 : 3,  15 : 3,  16 : 3,  17 : 3,  18 : 4,  19 : 4,  20  : 4,
  21 : 4,  22 : 4,  23 : 4,  24 : 4,  25 : 4,  26 : 4,  27 : 4,  28 : 4,  29 : 4,  30  : 4,
  31 : 4,  32 : 4,  33 : 4,  34 : 4,  35 : 4,  36 : 4,  37 : 4,  38 : 5,  39 : 5,  40  : 5,
  41 : 5,  42 : 5,  43 : 6,  44 : 7,  45 : 8,  46 : 8,  47 : 8,  48 : 8,  49 : 8,  50  : 8,
  51 : 8,  52 : 8,  53 : 8,  54 : 8,  55 : 8,  56 : 8,  57 : 8,  58 : 9,  59 : 10, 60  : 11, 
  61 : 12, 62 : 12, 63 : 12, 64 : 12, 65 : 12, 66 : 12, 67 : 12, 68 : 12, 69 : 12, 70  : 12, 
  71 : 12, 72 : 12, 73 : 12, 74 : 12, 75 : 12, 76 : 12, 77 : 12, 78 : 12, 79 : 12, 80  : 12, 
  81 : 12, 82 : 12, 83 : 12, 84 : 12, 85 : 12, 86 : 12, 87 : 12, 88 : 12, 89 : 12, 90  : 12, 
  91 : 12, 92 : 12, 93 : 12, 94 : 12, 95 : 12, 96 : 12, 97 : 12, 98 : 12, 99 : 12, 100 : 12
}

var shieldKinds = {
  1 : {name: 'Buckler', value: 165},
  2 : {name: 'Scudo leggero di legno', value: 153},
  3 : {name: 'Scudo leggero di metallo', value: 159},
  4 : {name: 'Scudo pesante di legno', value: 157},
  5 : {name: 'Scudo pesante di metallo', value: 170},
  6 : {name: 'Scudo torre', value: 180}
}

var randomShield = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1, 
  11 : 2, 12 : 2, 13 : 2, 14 : 2, 15 : 2, 16 : 3, 17 : 3, 18 : 3, 19 : 3, 20  : 3, 
  21 : 4, 22 : 4, 23 : 4, 24 : 4, 25 : 4, 26 : 4, 27 : 4, 28 : 4, 29 : 4, 30  : 4, 
  31 : 5, 32 : 5, 33 : 5, 34 : 5, 35 : 5, 36 : 5, 37 : 5, 38 : 5, 39 : 5, 40  : 5, 
  41 : 5, 42 : 5, 43 : 5, 44 : 5, 45 : 5, 46 : 5, 47 : 5, 48 : 5, 49 : 5, 50  : 5, 
  51 : 5, 52 : 5, 53 : 5, 54 : 5, 55 : 5, 56 : 5, 57 : 5, 58 : 5, 59 : 5, 60  : 5, 
  61 : 5, 62 : 5, 63 : 5, 64 : 5, 65 : 5, 66 : 5, 67 : 5, 68 : 5, 69 : 5, 70  : 5, 
  71 : 5, 72 : 5, 73 : 5, 74 : 5, 75 : 5, 76 : 5, 77 : 5, 78 : 5, 79 : 5, 80  : 5, 
  81 : 5, 82 : 5, 83 : 5, 84 : 5, 85 : 5, 86 : 5, 87 : 5, 88 : 5, 89 : 5, 90  : 5, 
  91 : 5, 92 : 5, 93 : 5, 94 : 5, 95 : 5, 96 : 6, 97 : 6, 98 : 6, 99 : 6, 100 : 6
}

var specialMaterials = {
  1 : {material: 'Adamantio'},
  2 : {material: 'Argento alchemico'},
  3 : {material: 'Ferro freddo'},
  4 : {material: 'Legnoscuro'},
  5 : {material: 'Mithral'},
  6 : {material: 'Pelle di drago'}
}

var minor = {
  1   : 1,  2  : 1,  3  : 1,  4  : 1,  5  : 1,  6  : 1,  7  : 1,  8  : 1,  9 : 1,
  10  : 1,  11 : 1,  12 : 1,  13 : 1,  14 : 1,  15 : 1,  16 : 1,  17 : 1,  18 : 1,  19 : 1,
  20  : 1,  21 : 1,  22 : 1,  23 : 1,  24 : 1,  25 : 1,  26 : 1,  27 : 1,  28 : 1,  29 : 1,
  30  : 1,  31 : 1,  32 : 1,  33 : 1,  34 : 1,  35 : 1,  36 : 1,  37 : 1,  38 : 1,  39 : 1,
  40  : 1,  41 : 1,  42 : 1,  43 : 1,  44 : 1,  45 : 1,  46 : 1,  47 : 1,  48 : 1,  49 : 1,
  50  : 1,  51 : 1,  52 : 1,  53 : 1,  54 : 1,  55 : 1,  56 : 1,  57 : 1,  58 : 1,  59 : 1,
  60  : 1,  61 : 2,  62 : 2,  63 : 2,  64 : 2,  65 : 2,  66 : 2,  67 : 2,  68 : 2,  69 : 2,
  70  : 2,  71 : 2,  72 : 2,  73 : 2,  74 : 2,  75 : 2,  76 : 2,  77 : 2,  78 : 2,  79 : 2,
  80  : 2,  81 : 3,  82 : 3,  83 : 3,  84 : 3,  85 : 3,  86 : 4,  87 : 4,  88 : 11, 89 : 11,
  90  : 12, 91 : 12, 92 : 13, 93 : 13, 94 : 13, 95 : 13, 96 : 13, 97 : 13, 98 : 13, 99 : 13, 
  100 : 13
}

var medium = {
  1  : 1,  2  : 1,  3  : 1,  4  : 1,  5  : 1,  6  : 2,  7  : 2,  8  : 2,  9  : 2,  10  : 2,
  11 : 3,  12 : 3,  13 : 3,  14 : 3,  15 : 3,  16 : 3,  17 : 3,  18 : 3,  19 : 3,  20  : 3,
  21 : 4,  22 : 4,  23 : 4,  24 : 4,  25 : 4,  26 : 4,  27 : 4,  28 : 4,  29 : 4,  30  : 4,
  31 : 5,  32 : 5,  33 : 5,  34 : 5,  35 : 5,  36 : 5,  37 : 5,  38 : 5,  39 : 5,  40  : 5,
  41 : 6,  42 : 6,  43 : 6,  44 : 6,  45 : 6,  46 : 6,  47 : 6,  48 : 6,  49 : 6,  50  : 6,
  51 : 7,  52 : 7,  53 : 7,  54 : 7,  55 : 7,  56 : 8,  57 : 8,  58 : 11, 59 : 11, 60  : 11, 
  61 : 12, 62 : 12, 63 : 12, 64 : 13, 65 : 13, 66 : 13, 67 : 13, 68 : 13, 69 : 13, 70  : 13, 
  71 : 13, 72 : 13, 73 : 13, 74 : 13, 75 : 13, 76 : 13, 77 : 13, 78 : 13, 79 : 13, 80  : 13, 
  81 : 13, 82 : 13, 83 : 13, 84 : 13, 85 : 13, 86 : 13, 87 : 13, 88 : 13, 89 : 13, 90  : 13, 
  91 : 13, 92 : 13, 93 : 13, 94 : 13, 95 : 13, 96 : 13, 97 : 13, 98 : 13, 99 : 13, 100 : 13
}

var major = {
  1  : 5,  2  : 5,  3  : 5,  4  : 5,  5  : 5,  6  : 5,  7  : 5,  8  : 5,  9  : 6,  10  : 6,
  11 : 6,  12 : 6,  13 : 6,  14 : 6,  15 : 6,  16 : 6,  17 : 7,  18 : 7,  19 : 7,  20  : 7,
  21 : 7,  22 : 7,  23 : 7,  24 : 7,  25 : 7,  26 : 7,  27 : 7,  28 : 8,  29 : 8,  30  : 8,
  31 : 8,  32 : 8,  33 : 8,  34 : 8,  35 : 8,  36 : 8,  37 : 8,  38 : 8,  39 : 9,  40  : 9,
  41 : 9,  42 : 9,  43 : 9,  44 : 9,  45 : 9,  46 : 9,  47 : 9,  48 : 9,  49 : 9,  50  : 10, 
  51 : 10, 52 : 10, 53 : 10, 54 : 10, 55 : 10, 56 : 10, 57 : 10, 58 : 11, 59 : 11, 60  : 11, 
  61 : 12, 62 : 12, 63 : 12, 64 : 13, 65 : 13, 66 : 13, 67 : 13, 68 : 13, 69 : 13, 70  : 13, 
  71 : 13, 72 : 13, 73 : 13, 74 : 13, 75 : 13, 76 : 13, 77 : 13, 78 : 13, 79 : 13, 80  : 13, 
  81 : 13, 82 : 13, 83 : 13, 84 : 13, 85 : 13, 86 : 13, 87 : 13, 88 : 13, 89 : 13, 90  : 13, 
  91 : 13, 92 : 13, 93 : 13, 94 : 13, 95 : 13, 96 : 13, 97 : 13, 98 : 13, 99 : 13, 100 : 13
}
