var utils = require('../utilities');

exports.all = function(){
  return specificWeapons;
}

exports.getRandomSpecificArmor = function(rarity){

  var rarity = rarity || 'minor';
  var num = utils.getRandomNumber();
  var armor;
 
  if(rarity = 'minor'){
    armor = specificWeapons[minor[num]];
  } else if (rarity = 'medium'){
    armor = specificWeapons[medium[num]];
  } else if (rarity = 'major'){
    armor = specificWeapons[major[num]];
  } else {
    throw new Error ("rarity must be 'minor', 'medium' or 'major'");
  }

  return armor

}

var specificWeapons = {
  1 : {name: 'Freccia del sonno', value: 132},
  2 : {name: 'Quadrello urlante', value: 267},
  3 : {name: 'Pugnale d\'argento perfetto', value: 322},
  4 : {name: 'Spada lunga di ferro freddo perfetta', value: 330},
  5 : {name: 'Giavellotto del fulmine', value: 1500},
  6 : {name: 'Freccia assassina', value: 2282},
  7 : {name: 'Pugnale in adamantio', value: 3002},
  8 : {name: 'Ascia da battaglia in adamantio', value: 3010},
  9 : {name: 'Freccia assassina (superiore)', value: 4057},
  10 : {name: 'Spina frantumante', value: 4315},
  11 : {name: 'Pugnale avvelenato', value: 8302},
  12 : {name: 'Tridente dell\'avvertimento', value: 10115},
  13 : {name: 'Sventura del mutaforma', value: 12780},
  14 : {name: 'Pugnale dell\'assassino', value: 18302},
  15 : {name: 'Tridente del comando dei pesci', value: 18650},
  16 : {name: 'Spada lingua di fuoco', value: 20715},
  17 : {name: 'Lama della fortuna (0 desideri)', value: 22060},
  18 : {name: 'Spada della furtività', value: 22310},
  19 : {name: 'Spada dei piani', value: 22315},
  20 : {name: 'Spada ruba nove vite', value: 23057},
  21 : {name: 'Spada del furto vitale', value: 25715},
  22 : {name: 'Arco del giuramento', value: 25600},
  23 : {name: 'Mazza del terrore', value: 38552},
  24 : {name: 'Prosciuga-vita', value: 40320},
  25 : {name: 'Scimitarra silvana', value: 47315},
  26 : {name: 'Stocco pungente', value: 50320},
  27 : {name: 'Lama del sole', value: 50335},
  28 : {name: 'Spadone del gelo', value: 54475},
  29 : {name: 'Martello nanico da lancio', value: 60312},
  30 : {name: 'Lama della fortuna (1 desiderio)', value: 62360},
  31 : {name: 'Mazza della punizione', value: 75312},
  32 : {name: 'Lama della fortuna (2 desideri)', value: 102660},
  33 : {name: 'Sacro vendicatore', value: 120630},
  34 : {name: 'Lama della fortuna (3 desideri)', value: 142960}   
}

var minor = {
  1  : 1, 2  : 1, 3  : 1, 4  : 1, 5  : 1, 6  : 1, 7  : 1, 8  : 1, 9  : 1, 10  : 1,
  11 : 1, 12 : 1, 13 : 1, 14 : 1, 15 : 1, 16 : 2, 17 : 2, 18 : 2, 19 : 2, 20  : 2, 
  21 : 2, 22 : 2, 23 : 2, 24 : 2, 25 : 2, 26 : 3, 27 : 3, 28 : 3, 29 : 3, 30  : 3, 
  31 : 3, 32 : 3, 33 : 3, 34 : 3, 35 : 3, 36 : 3, 37 : 3, 38 : 3, 39 : 3, 40  : 3, 
  41 : 3, 42 : 3, 43 : 3, 44 : 3, 45 : 3, 46 : 4, 47 : 4, 48 : 4, 49 : 4, 50  : 4, 
  51 : 4, 52 : 4, 53 : 4, 54 : 4, 55 : 4, 56 : 4, 57 : 4, 58 : 4, 59 : 4, 60  : 4, 
  61 : 4, 62 : 4, 63 : 4, 64 : 4, 65 : 4, 66 : 5, 67 : 5, 68 : 5, 69 : 5, 70  : 5, 
  71 : 5, 72 : 5, 73 : 5, 74 : 5, 75 : 5, 76 : 6, 77 : 6, 78 : 6, 79 : 6, 80  : 6, 
  81 : 7, 82 : 7, 83 : 7, 84 : 7, 85 : 7, 86 : 7, 87 : 7, 88 : 7, 89 : 7, 90  : 7, 
  91 : 8, 92 : 8, 93 : 8, 94 : 8, 95 : 8, 96 : 8, 97 : 8, 98 : 8, 99 : 8, 100 : 8 
}

var medium = {
  1  : 5,  2  : 5,  3  : 5,  4  : 5,  5  : 5,  6  : 5,  7  : 5,  8  : 5,  9  : 5,  10  : 6,
  11 : 6,  12 : 6,  13 : 6,  14 : 6,  15 : 6,  16 : 7,  17 : 7,  18 : 7,  19 : 7,  20  : 7, 
  21 : 7,  22 : 7,  23 : 7,  24 : 7,  25 : 8,  26 : 8,  27 : 8,  28 : 8,  29 : 8,  30  : 8, 
  31 : 8,  32 : 8,  33 : 8,  34 : 9,  35 : 9,  36 : 9,  37 : 9,  38 : 10, 39 : 10, 40  : 10, 
  41 : 11, 42 : 11, 43 : 11, 44 : 11, 45 : 11, 46 : 11, 47 : 12, 48 : 12, 49 : 12, 50  : 12, 
  51 : 12, 52 : 13, 53 : 13, 54 : 13, 55 : 13, 56 : 13, 57 : 14, 58 : 14, 59 : 14, 60  : 14, 
  61 : 14, 62 : 14, 63 : 15, 64 : 15, 65 : 15, 66 : 15, 67 : 16, 68 : 16, 69 : 16, 70  : 16, 
  71 : 16, 72 : 16, 73 : 16, 74 : 16, 75 : 17, 76 : 17, 77 : 17, 78 : 17, 79 : 17, 80  : 18, 
  81 : 18, 82 : 18, 83 : 18, 84 : 18, 85 : 18, 86 : 18, 87 : 19, 88 : 19, 89 : 19, 90  : 19, 
  91 : 19, 92 : 20, 93 : 20, 94 : 20, 95 : 20, 96 : 21, 97 : 21, 98 : 21, 99 : 22, 100 : 22 
}

var major = {
  1  : 13, 2  : 13, 3  : 13, 4  : 14, 5  : 14, 6  : 14, 7  : 14, 8  : 15, 9  : 15, 10  : 16,
  11 : 16, 12 : 16, 13 : 16, 14 : 17, 15 : 17, 16 : 17, 17 : 17, 18 : 18, 19 : 18, 20  : 18, 
  21 : 18, 22 : 18, 23 : 18, 24 : 18, 25 : 19, 26 : 19, 27 : 19, 28 : 19, 29 : 19, 30  : 19, 
  31 : 19, 32 : 20, 33 : 20, 34 : 20, 35 : 20, 36 : 20, 37 : 20, 38 : 21, 39 : 21, 40  : 21, 
  41 : 21, 42 : 21, 43 : 22, 44 : 22, 45 : 22, 46 : 22, 47 : 23, 48 : 23, 49 : 23, 50  : 23, 
  51 : 23, 52 : 24, 53 : 24, 54 : 24, 55 : 24, 56 : 24, 57 : 24, 58 : 25, 59 : 25, 60  : 25, 
  61 : 25, 62 : 25, 63 : 26, 64 : 26, 65 : 26, 66 : 26, 67 : 26, 68 : 27, 69 : 27, 70  : 27, 
  71 : 27, 72 : 27, 73 : 27, 74 : 28, 75 : 28, 76 : 28, 77 : 28, 78 : 28, 79 : 28, 80  : 29, 
  81 : 29, 82 : 29, 83 : 29, 84 : 29, 85 : 30, 86 : 30, 87 : 30, 88 : 30, 89 : 30, 90  : 30, 
  91 : 30, 92 : 31, 93 : 31, 94 : 31, 95 : 31, 96 : 32, 97 : 32, 98 : 33, 99 : 33, 100 : 34 
}
