var utils = require('./utilities');

exports.all = function(){
  return gems;
}

exports.byRarity = function(number){
  return gems[number];
}

exports.getRandomGem = function(){

  var rarity = utils.getGemRarity();

  var gemJson = gems[rarity];
  var value = utils.calculateValue(gemJson.value);
  var name = utils.findName(gemJson.names);

  var gem = {
    name    : name,
    dice    : gemJson.valueStr,
    value   : value,
    avarage : gemJson.avarage
  }

  return gem;

}

exports.getSpecificNumberOfGems = function(number){

  var array = [];

  for(i=0; i<number; i++){
    array.push(this.getRandomGem());
  }

  return array;

}


var gems = {
  1 : {
    range   : '1-25',
    avarage : '10 mo',
    valueStr: '4d4 mo',
    value   : '4-4-1',
    names   : ['Agata striata', 'Azzurrite', 'Quarzo blu', 'Ematite', 'Lapislazzulo', 'Malachite', 'Ossidiana', 'Rodocrosite', 'Turchese occhio di tigre', 'Perla di fiume (irregolare)']
  },
  2 : {
    range   : '26-50',
    avarage : '50 mo',
    valueStr: '2d4x10 mo',
    value   : '2-4-10',
    names   : ['Eliotropia', 'Corniola', 'Calcedonio', 'Crisoprasio', 'Citrino', 'Iolito', 'Diaspro', 'Selenite', 'Onice', 'Crisolito', 'Cristallo di roccia (quarzo chiaro)', 'Sardonica', 'Sardonite', 'Quarzo rosato', 'Quarzo affumicato', 'Quarzo rosa di stella', 'Zircone']
  },
  3 : {
    range   : '51-70',
    avarage : '100 mo',
    valueStr: '4d4x10 mo',
    value   : '4-4-10',
    names   : ['Ambra', 'Ametista', 'Crisoberillo', 'Corallo', 'Granato rosso', 'Granato verde-marrone', 'Giada', 'Giaietto', 'Perla bianca', 'Perla dorata', 'Perla rosa', 'Perla argentata', 'Spinello rosso', 'Spinello rosso-marrone', 'Spinello verde scuro', 'Tormalina']
  },
  4 : {
    range   : '71-90',
    avarage : '500 mo',
    valueStr: '2d4x100 mo',
    value   : '2-4-100',
    names   : ['Alessandrite', 'Acquamarina', 'Granato viola', 'Perla nera', 'Spinello blu scuro', 'Topazio giallo oro']
  },
  5 : {
    range   : '91-99',
    avarage : '1000 mo',
    valueStr: '4d4x100 mo',
    value   : '4-4-100',
    names   : ['Smeraldo', 'Opale bianco', 'Opale nero', 'Opale di fuoco', 'Zaffiro blu', 'Corindone giallo fuoco', 'Corindone vermiglio', 'Zaffiro a stella blue e nero', 'Rubino a stella']
  },
  6 : {
    range   : '100-100',
    avarage : '5000 mo',
    valueStr: '2d4x1000 mo',
    value   : '2-4-1000',
    names   : ['Smeraldo verde brillante cristallino', 'Diamante bianco-blu', 'Diamante giallo canarino', 'Diamante rosa', 'Diamante marrone', 'Diamante blu', 'Giacinto']
  }
}