exports.calculateValue = function(valueString){

  var data = valueString.split('-');
  var i = 0;
  var value = 0;

  for(i=0; i<data[0]; i++){
    value += Math.ceil(Math.random()*data[1]);
  } 

  return value*data[2]

}


exports.findName = function(arrayOfNames){

  var numberOfNames = arrayOfNames.length;
  var randomNumber = Math.floor(Math.random()*numberOfNames);

  return arrayOfNames[randomNumber];

}

exports.getRandomNumber = function(num){
  var num = num || 100;
  return randomNumber = Math.ceil(Math.random()*num);
}

exports.getAbilities = function(){
  
  var ok = true;
  var abilities = [];
  
  while (ok) {

    num = this.getRandomNumber();
    num1 = this.getRandomNumber();

    if(num !== 100){

      abilities.push(num);
      ok = false;
    }

    if(num1 !== 100){
      abilities.push(num1);
      ok = false;
    }

    if(num === 100 || num1 === 100){
      ok = true;
    }

  }

  return abilities;

}

exports.getGemRarity = function(){
  
  var randomNumber = Math.ceil(Math.random()*100);
  var rarity = 1;

  if(randomNumber <= 25){
    rarity = 1;
  } else if(randomNumber <= 50){
    rarity = 2;
  } else if (randomNumber <= 70){
    rarity = 3;
  } else if (randomNumber <= 90){
    rarity = 4;
  } else if (randomNumber <= 99){
    rarity = 5;
  } else if (randomNumber == 100){
    rarity = 6;
  }

  return rarity;

}

exports.getArtRarity = function(){
  
  var randomNumber = Math.ceil(Math.random()*100);
  var rarity = 1;

  if(randomNumber <= 10){
    rarity = 1;
  } else if(randomNumber <= 25){
    rarity = 2;
  } else if (randomNumber <= 40){
    rarity = 3;
  } else if (randomNumber <= 50){
    rarity = 4;
  } else if (randomNumber <= 60){
    rarity = 5;
  } else if (randomNumber <= 70){
    rarity = 6;
  } else if (randomNumber <= 80){
    rarity = 7;
  } else if (randomNumber <= 85){
    rarity = 8;
  } else if (randomNumber <= 90){
    rarity = 9;
  } else if (randomNumber <= 95){
    rarity = 10;
  } else if (randomNumber <= 99){
    rarity = 11;
  } else if (randomNumber == 100){
    rarity = 12;
  }

  return rarity;

}


exports.getCommonItemRarity = function(){
  
  var randomNumber = Math.ceil(Math.random()*100);
  var rarity = 1;

  if(randomNumber <= 17){
    rarity = 1;
  } else if(randomNumber <= 50){
    rarity = 2;
  } else if (randomNumber <= 83){
    rarity = 3;
  } else {
    rarity = 4;
  }

  return rarity;

}

exports.getWeaponKind = function(){
  

  var rndNum = Math.ceil(Math.random()*100);
  var weaponKind;
  
  if(rndNum <= 50){
    weaponKind = 'commonMelee';
  } else if (rndNum <= 70){
    weaponKind = 'uncommonMelee';
  } else {
    weaponKind = 'commonRanged';
  }

  return weaponKind;

}